from flask import Flask, jsonify
from auth import auth, login_required

app = Flask(__name__)
app.register_blueprint(auth)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
@app.route('/')
@login_required
def hello():
    # return 'Hello %s!'
    return jsonify(
        id= "file",
        type= "File",
        popup= {}
    )

if __name__ == '__main__':
    app.run()
