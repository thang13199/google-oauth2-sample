from flask import redirect, url_for, session, request, jsonify, Blueprint, g
from functools import wraps
from flask_oauthlib.client import OAuth
import time

auth = Blueprint('auth', __name__)
oauth = OAuth()
SIGNED_OUT_PAGE = 'https://affiliateservices.gecompany.com/logoff/logoff.jsp'

# carbon = oauth.remote_app(
#     'carbon_sequencing_staging',
#     consumer_key='carbon_sequencing_staging',
#     consumer_secret='3ADA8622421E59FD2AC8B6E18BCD3',
#     request_token_params={'scope': 'openid'},
#     base_url='https://fssfed.stage.ge.com',
#     request_token_url=None,
#     access_token_method='POST',
#     access_token_url='https://fssfed.stage.ge.com/fss/as/token.oauth2',
#     authorize_url='https://fssfed.stage.ge.com/fss/as/authorization.oauth2'
# )

carbon = oauth.remote_app(
    'carbon_sequencing_staging',
    consumer_key='161539656692-c490arpgnr1ecn9ook3ls28799q114d7.apps.googleusercontent.com',
    consumer_secret='iB2tHWXH_l7hrJgDbXyPkKKP',
    request_token_params={'scope': 'email'},
    base_url='https://www.googleapis.com/oauth2/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
)


@auth.route('/auth/callback')
def callback():
    return carbon.authorize(callback=url_for('auth.authorized', _external=True))


@auth.route('/auth/authorized')
def authorized():
    resp = carbon.authorized_response()
    if resp is None or resp.get('access_token') is None:
        return 'Access denied: reason=%s error=%s resp=%s' % (
            request.args['error'],
            request.args['error_description'],
            resp
        )

    session['access_token'] = resp['access_token']

    present_unix_timestamp = int(time.time())
    expires_at = present_unix_timestamp + resp['expires_in']
    session['expires_at'] = expires_at

    return redirect('http://127.0.0.1:5000/')


@auth.route('/auth/logout')
def logout():
    session['access_token'] = None
    session['expires_at'] = None
    return redirect(SIGNED_OUT_PAGE)


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if token_invalid():
            return redirect(url_for('auth.callback'))
        return f(*args, **kwargs)

    return decorated_function


def token_invalid():
    if 'access_token' not in session or session['access_token'] is None:
        return True
    if 'expires_at' not in session or session['expires_at'] is None:
        return True
    print(session['access_token'])
    present_unix_timestamp = int(time.time())
    if present_unix_timestamp > int(session['expires_at']):
        return True
