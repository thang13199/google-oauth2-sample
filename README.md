# sample sso with google oauth2 - For reference only
- ```bash
  $ python3 app.py                                                  
  * Serving Flask app 'app' (lazy loading)
  * Environment: production
    WARNING: This is a development server. Do not use it in a production deployment.
    Use a production WSGI server instead.
  * Debug mode: off
  * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
    ```
- This project is SSO sample app using logic oauth2 (same with SSO GE - only edit the url, key, secret)
- If you do not have an account to test the logic, you can use google oauth 2 to try on local environment before integrating into the system
![img.png](img.png)

- To get the key and secret, config callback url on google api using [this youtube link](https://www.youtube.com/watch?v=xH6hAW3EqLk&t=181s) and [this document](https://developers.google.com/identity/protocols/oauth2)
- If you have any question about my code, you can create gitlab issue in this project
- you can use my secret and key if your callback url is same as mine, so you can use only this logic code instead of create your secret on google: 
![img_1.png](img_1.png)
- 
